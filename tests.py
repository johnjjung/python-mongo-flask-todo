import requests
from nose.tools import assert_true, assert_equals

def test_create_item():
    task = { "task": "Take out trash" }
    resp = requests.post('http://localhost:8080/todos', data=task)
    if resp.status_code != 200:
        raise Exception('POST /todos/ {}'.format(resp.status_code))
    # print('Created task. ID: {}'.format(resp.json()["item_id"]))
    item = resp.json()
    print item
    assert_true(resp.json()["item"])

def test_get_items():
    resp = requests.get('http://localhost:8080/todos')
    if resp.status_code != 200:
        raise Exception('GET /todos/ {}'.format(resp.status_code))
    assert_true(resp.json())

def test_get_item():
    # get an item from the list
    resp = requests.get('http://localhost:8080/todos')
    first_item = resp.json()[0]['_id']['$oid']
    resp = requests.get('http://localhost:8080/todos/' + first_item)
    if resp.status_code != 200:
        raise Exception('GET /todos/ {}'.format(resp.status_code))
    assert_true(resp.json())

def test_update_item():
    # get an item from the list
    resp = requests.get('http://localhost:8080/todos')
    first_item = resp.json()[-1]['_id']['$oid']
    task = { "_id" : first_item, "item": "Buy Groceries" }
    # update that task
    resp = requests.put('http://localhost:8080/todos/' + first_item, data=task)
    if resp.status_code != 200:
        raise Exception('PUT /todos/' + first_item +' {}'.format(resp.status_code))
    assert_true(resp.json()['updated'])

def test_delete_item():
    # get an item from the list
    resp = requests.get('http://localhost:8080/todos')
    first_item = resp.json()[-1]['_id']['$oid']
    task = { "_id" : first_item, "item": "Buy Groceries" }
    # update that task
    resp = requests.delete('http://localhost:8080/todos/' + first_item, data=task)
    if resp.status_code != 200:
        raise Exception('PUT /todos/' + first_item +' {}'.format(resp.status_code))
    assert_true(resp.json()['deleted'])