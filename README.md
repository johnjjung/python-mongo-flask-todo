# Simple Flask Mongo TODO app #
This is a demo repo to demonstrate a simple RESTFUL flask app with mongodb that is AWS EB ready.

**Update**
I refactored so that the folder structures so that we follow SOLID & MVC with more modularized code where the models utilize Mongo Modeling ORM (mongoengine) MongoKit Example is also demonstrated commented out. All tests were utilized and passing. Used Mongoengine save only for create for demo purposes.


### Endpoints ###

**Get a list of Tasks**

[GET] http://localhost:8080/todos


```
GET /todos HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
```

```
[
  {
    "_id": {
      "$oid": "56ed9a98522389b013f4a23e"
    },
    "task": "take out the garbage"
  },
  {
    "_id": {
      "$oid": "56ed9ab5522389b013f4a23f"
    },
    "task": "go to the museum"
  }
]
```

**Create a task**

[POST] http://localhost:8080/todos

[body form-data]task=Check Mail

```
POST /todos HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW

----WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="task"

go to post office
----WebKitFormBoundary7MA4YWxkTrZu0gW
```

```
{
  "task_id": "56ed9ab5522389b013f4a23f"
}
```

**Read a task**

[GET] http://localhost:8080/todos/{task_id}

```
GET /todos/56ed85cf52238988f60d68e6 HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
```


**Update a task**

[PUT] http://localhost:8080/todos/{task_id}

```
PUT /todos/56ed89785223898c2c276f3d HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW

----WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="task"

go to post offices
----WebKitFormBoundary7MA4YWxkTrZu0gW
```

```
{
  "updated": true
}
```

**DELETE a task**

[DELETE] http://localhost:8080/todos/{task_id}

```
DELETE /todos/56ed85e852238988f60d68e7 HTTP/1.1
Host: localhost:8080
Cache-Control: no-cache
Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW

----WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="task"

go to post offices
----WebKitFormBoundary7MA4YWxkTrZu0gW
```

```
{
  "deleted": true
}
```

## Things I was paying attention on ##

### git + gitflow ###
* Using git effectively in a team environment by using gitflow techniques. Managing master,develop, and feature branches.
* Using rebranching and merging back into develop effectively
* Ability to deal with merge conflicts without losing important code
* Use of release branches to make deployment level changes, that merges with master

### Flask Directory Structure ###
* Aware of directory large scale flask directory structures
* venv directory is gitignore but used for keeping dependencies in check
* requirements.txt for pip installs and deployment
* use of single responsibility config file
* use of environment variables for AWS

[Flask App Builder](https://github.com/dpgaspar/Flask-AppBuilder)
[Flask Foundation](https://jackstouffer.github.io/Flask-Foundation)
[Flask Bones](https://github.com/cburmeister/flask-bones)

### Use of Libraries ###
* PyMongo: aware of ORM's such as MongoEngine/MongoKit, also mongodb 3+ supports validation
* Flask-Pymongo: recommended way for python to communicate with mongodb
* Flask-RESTful: great for resourceful routing
* flask-sandboy: another great possible testing suite
* celery: thinking of doing a simple queue
* rabbitmq: thinking of doing a simple queue

### Testing ###
* Using a very simple nose test with assert_equals and requests
* Aware that there are much more robust methods for automated RESTFUL testing such as:
[PyRestTest](https://github.com/svanoort/pyresttest)
[Flask API](http://www.flaskapi.org/)
* Aware for webpages we can use jasmine, selenium, etc.. for BDD

## Cavets ##
This is for demonstration purposes only, there are many ways to improve this and open for discussion and critique.
* did not start with Foundation Flask app, because did not want scaffolding and all the tests that come with it.