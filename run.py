from app import application


# run the app.
if __name__ == "__main__":
    # Setting debug to True enables debug output. This line should be
    # removed before deploying a production app.
    application.debug = True
    application.config.from_object('config')
    # development server
    application.run(host='0.0.0.0', port=8080, debug=True)
    # application.run()