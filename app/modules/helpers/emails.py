
# Email function - example helper methods
def send_email(msg, from_email, to_email, subject):
  import smtplib
  from email.mime.text import MIMEText

  msg = MIMEText(msg)
  msg['Subject'] = subject
  msg['From'] = from_email
  msg['To'] = to_email

  try:
    # Production
    s = smtplib.SMTP('localhost')
  except:
    # Testing
    s = smtplib.SMTP('smtp.gmail.com:587')
    s.starttls()
    s.login(gmail_user(), gmail_pwd())

  s.sendmail(from_email, [to_email], msg.as_string())
  s.quit()