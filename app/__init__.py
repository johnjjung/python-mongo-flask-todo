# Import flask
from flask import Flask, jsonify, Response, render_template, Blueprint
from flask_restful import Resource, Api, reqparse
from app.controllers.tasks_controller import *

# EB looks for an 'application' callable by default.
application = Flask(__name__)
api = Api(application)

@application.route("/")
def hello():
    return render_template('layouts/default.html')

# Resourceful routing
# application.register_blueprint(main)
api.add_resource(TodoItem, '/todos/<string:todo_id>')
api.add_resource(TodoList, '/todos')