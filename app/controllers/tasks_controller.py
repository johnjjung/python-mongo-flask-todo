from flask import jsonify, Response, Blueprint, request
from bson.objectid import ObjectId
from flask_restful import Resource, reqparse
from bson.json_util import dumps
from flask_restful import Resource, Api, reqparse
from app.models.Task import *
from pprint import pprint

parser = reqparse.RequestParser()
parser.add_argument('task')

class TodoItem(Resource):

    # Read item
    def get(self, todo_id):
        task = mongo.task.find({ "_id" : ObjectId(todo_id)})
        return Response(dumps(task), mimetype='application/json')
    # Update item
    def put(self, todo_id):
        # See cavets section about what needs to be done
        args = parser.parse_args()
        task = mongo.task.update_one(
            { "_id" : ObjectId(todo_id)},
            { "$set": args }
        )
        return jsonify({ "updated" : task.matched_count == 1 })
    # Delete item
    def delete(self, todo_id):
        task = mongo.task.delete_one(
            { "_id" : ObjectId(todo_id)}
        )
        return jsonify({ "deleted" : task.deleted_count == 1 })

class TodoList(Resource):

   # Get a list of items
    def get(self):
        todos = mongo.task.find()
        return Response(dumps(todos), mimetype='application/json')

    # Create item
    def post(self):
        args = parser.parse_args()
        task = Task(item=args['task'])
        task = task.save()
        return Response(dumps(task._data), mimetype='application/json')


