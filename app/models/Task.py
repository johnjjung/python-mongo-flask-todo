# MongoEngine to model Task
from mongoengine import *

connect('app')

class Task(Document):
    item = StringField(required=True)


# MongoKit Modeling Example
# from mongokit import *
# from flask.ext.mongokit import MongoKit
# mongo = MongoKit(application)
# @mongo.register
# class Task(Document):
#     __database__ = 'app'
#     __collection__ = 'tasks'
#     structure = {
#         "item" : unicode
#     }
#     required_fields = ['item']


# Working example with Pymongo
from pymongo import MongoClient
mongo = MongoClient('localhost', 27017).app
